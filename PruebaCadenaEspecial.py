'''
Created on 7/09/2021

@author: Herrera
'''
import random
import time
from io import StringIO



class CadenaEspecial:
    def __init__(self, cadena):
        self.cadena = cadena
    
    def invertirCadenas(self):
        cadenaInvertida = self.cadena[::-1]
        return cadenaInvertida
    
    def invertirPalabras(self):
        cadenas = self.cadena.split(" ")
        nuevaCadena = ""
        for i in range(len(cadenas)-1, -1, -1):
            nuevaCadena += cadenas[i] + " "
        return nuevaCadena
    
    def agregarCaracteres(self, posicion, subcadena):
        nuevaCadena = ""
        for i in range(len(cadena)):
            if (posicion == i):
                nuevaCadena += cadena[i]
                nuevaCadena += subcadena
            else:
                nuevaCadena += cadena[i]
        return nuevaCadena
    
    def eliminarCaracteres(self, posicion, numeroLetras):
        nuevaCadena = ""
        
        for i in range(len(cadena)):
            if (i>= posicion-1 and i<= (posicion+numeroLetras-1)):
                i=i
            else:
                nuevaCadena += cadena[i]
        return nuevaCadena;  
    
    def mostrarCadenaCamelCase(self):
        nuevaCadena = ""
        for i in range(len(cadena)):
            if(i%2 == 0):
                nuevaCadena += cadena[i].upper()
            else:
                nuevaCadena += cadena[i].lower()
        return nuevaCadena
    
    def mostrarPrimeraMayuscula(self):
        nuevaCadena = ""
        cadenas = cadena.split(" ")
        for i in range(len(cadenas)):
            for j in range(len(cadenas[i])):
                if (j == 0):
                    nuevaCadena += cadenas[i][j].upper()
                else:
                    nuevaCadena += cadenas[i][j].lower()
            nuevaCadena += " "
        return nuevaCadena
    
    def generarVectorRandom(self):
        vector = []
        nc = ""
        for i in range(350000):
            i=i
            nc=""
            primerNumero = random.randrange(0, 99, 1)
            if (primerNumero <= 9):
                nc += "0" + str(primerNumero)
            else:
                nc += str(primerNumero)
            if (primerNumero <= 20):
                nc += "070"
            elif (primerNumero > 20 and primerNumero <= 40):
                nc += "071"
            elif (primerNumero > 40 and primerNumero <= 60):
                nc += "072"
            elif (primerNumero > 60 and primerNumero <= 80):
                nc += "073"
            elif (primerNumero > 80 and primerNumero <= 99):
                nc+="074"
            else:
                nc += "070"
            tercerNumero = random.randrange(0, 999, 1)
            if (tercerNumero <= 9):
                nc += "00" + str(tercerNumero)
            elif (tercerNumero >= 10 and tercerNumero <= 99):
                nc += "0" + str(tercerNumero)
            else:
                nc += str(tercerNumero)
            vector.append(str(nc));  
        return vector
    
    def generarVectorRandomStringBuilder(self):
        vector = []
        nc = ""
        for i in range(350000):
            i=i
            nc = StringBuilder()
            primerNumero = random.randrange(0, 99, 1)
            if (primerNumero <= 9):
                nc.Add(("0" + str(primerNumero)))
            else:
                nc.Add(str(primerNumero))
            if (primerNumero <= 20):
                nc.Add("070")
            elif (primerNumero > 20 and primerNumero <= 40):
                nc.Add("071")
            elif (primerNumero > 40 and primerNumero <= 60):
                nc.Add("072")
            elif (primerNumero > 60 and primerNumero <= 80):
                nc.Add("073")
            elif (primerNumero > 80 and primerNumero <= 99):
                nc.Add("074")
            else:
                nc.Add("070")
            tercerNumero = random.randrange(0, 999, 1)
            if (tercerNumero <= 9):
                nc.Add(("00" + str(tercerNumero)))
            elif (tercerNumero >= 10 and tercerNumero <= 99):
                nc.Add(("0" + str(tercerNumero)))
            else:
                nc.Add(str(tercerNumero))
            vector.append(nc);  
        return vector
    
    def agregarLetrasNumeroControl(self, vector):
        numeros = ""
        
        inicio = time.time()
        
        for i in range(len(vector)):
            numeros = ""
            numeros = str(vector[i][3]) + str(vector[i][4])
            if (numeros == "70"):
                vector[i] = "S" + vector[i]
            elif (numeros == "71"):
                vector[i]= "M" + vector[i]
            elif (numeros == "72"):
                vector[i] = "A" + vector[i]
            elif (numeros == "73"):
                vector[i]= "L" + vector[i]
            elif (numeros == "74"):
                vector[i] = "C" + vector[i]
            else:
                vector[i] = "S" + vector[i]
        
        fin = time.time()
        print(f"\nTiempo de ejecucion: {fin-inicio}s")
        
        return vector
    
    def agregarLetrasNumeroControlStringBuilder(self, vector):
        numeros = ""
        
        inicio = time.time()
        
        for i in range(len(vector)):
            numeros = ""
            str(vector[i])
            print(vector[i].___str__())
            if (numeros == "70"):
                vector[i].join("S")
            elif (numeros == "71"):
                vector[i]= "M" + vector[i]
            elif (numeros == "72"):
                vector[i] = "A" + vector[i]
            elif (numeros == "73"):
                vector[i]= "L" + vector[i]
            elif (numeros == "74"):
                vector[i] = "C" + vector[i]
            else:
                vector[i] = "S" + vector[i]
        
        fin = time.time()
        print(f"\nTiempo de ejecucion: {fin-inicio}s")
        
        return vector

class StringBuilder:
    _file_str = None

    def __init__(self):
        self._file_str = StringIO()

    def Add(self, str):
        self._file_str.write(str)

    def __str__(self):
        return self._file_str.getvalue()


cadena = input("Introduce cadena a analizar: ")
ce = CadenaEspecial(cadena)
opcion = 0
while(opcion != 8):
    print("\nElige una de las siguientes opciones");
    print("1) Mostrar la cadena invertida por letra ");
    print("2) Mostrar la cadena con palabras invertidas");
    print("3) Agregar/ caracteres y/o subcadenas en posiciones especificas");
    print("4) eliminar caracteres y/o subcadenas en posiciones especificas");
    print("5) Mostrar cadena en formato CaMeL CaSe  especial");
    print("6) Mostar la cadena con la primer letra de cada palabra en mayuscula");
    print("7) llenar vector con 35k de nc y agregar letra");
    print("8) Salir")
    opcion = int(input("introduce opcion: "))
    
    if(opcion == 1):
        print(ce.invertirCadenas())
    elif (opcion == 2):
        print(ce.invertirPalabras())
    elif (opcion == 3):
        posicion = input("Introduce pocicion: ")
        subCadena = input("Introduce subcadena: ")
        
        if(posicion <= len(ce.cadena)):
            print(ce.agregarCaracteres(posicion-1, subCadena))
        else:
            print(">> posicion erronea")

    elif (opcion == 4):
        posicion = input("Introduce pocicion: ")
        numeroLetras = input("Introduce subcadena: ")
        if(posicion <= len(ce.cadena)):
            print(ce.eliminarCaracteres(posicion-1, numeroLetras))
        else:
            print(">> posicion erronea")    
        
    elif (opcion == 5):
        print(ce.mostrarCadenaCamelCase())
    elif (opcion == 6):
        print(ce.mostrarPrimeraMayuscula())
    elif (opcion == 7):
        
        vector = ce.generarVectorRandom()
        vector = ce.agregarLetrasNumeroControl(vector)
        
        print("\nPrimeros 10 numeros")
        for i in range(10):
            print(vector[i])
    elif (opcion == 8):
        print("Saliendo . . .")
    else:
        print(">> Opcion incorrecta")
    print()
        
    
    




        
    